package com.example.pocketscience;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class MainActivity extends AppCompatActivity {
    private void createInitialFileForTesting() {
        /* TODO remove this initial testing shit */

        File dir = getFilesDir();
        File[] files = dir.listFiles();

        if (files.length == 0) {
            System.out.println("fuck creating file");
            try {
                FileOutputStream fo = openFileOutput("testfile", MODE_PRIVATE);
                PrintStream ps = new PrintStream(fo);
                ps.println("Hey ho my file");
            } catch (Exception e) {
                System.out.println("fuck: error in opening file");
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO refactor this piece of shit method

        // Default setup shit TODO
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Some toolbar shit TODO
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create a data record
        createInitialFileForTesting();

        // Add item to the list
        RecyclerView rv = findViewById(R.id.records_list);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(new RecordsListAdapter(getFilesDir()));

        // Set handler function for the small floating button
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Hey ho?", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
