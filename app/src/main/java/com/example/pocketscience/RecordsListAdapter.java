package com.example.pocketscience;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.File;


public class RecordsListAdapter extends RecyclerView.Adapter {
    private File directory;
    private File[] recordFiles;

    public RecordsListAdapter(File directory) {
        super();
        this.directory = directory;
        scanForDataRecords();
    }

    private void scanForDataRecords() {
        recordFiles = directory.listFiles();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecordsListViewHolder(new TextView(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RecordsListViewHolder vh = (RecordsListViewHolder)holder; // TODO is this unsafe?
        vh.setText(recordFiles[position].getName());
    }

    @Override
    public int getItemCount() {
        if (recordFiles != null)
            return recordFiles.length;
        else
            return 0;
    }

    public static class RecordsListViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;

        private void setText(String text) {
            textView.setText(text);
        }

        public RecordsListViewHolder(View v) {
            super(v);
            textView = (TextView)v; // TODO casts and assumptions suck
        }
    }
}
